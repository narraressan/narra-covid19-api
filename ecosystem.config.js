const envs = require('./config');

module.exports = {
  apps: [{
    name: 'API',
    script: './app/index.js',
    instances: 1,
    autorestart: true,
    watch: true,
    ignore_watch: ['./node_modules', './tests', './docker', './logs'],
    env: envs,
  }, {
    name: 'DB_SETUP',
    script: './models/db_setup.js',
    env: envs,
    instances: 1,
    autorestart: false,
    watch: false,
  }, {
    name: 'CRAWLERS',
    script: './routines/crawler.js',
    cron_restart: '0 * * * */7', // At minute 0 on every 7th day-of-week.
    env: envs,
    instances: 1,
    watch: true,
  }],
};
