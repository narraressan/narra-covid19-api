// used by sequelize migration
const db_config = require('./config.json');

module.exports = {
  DB_HOST: process.env.DB_HOST || db_config.generic.host,
  DB_PORT: process.env.DB_PORT || db_config.generic.port,
  DB_USER: process.env.DB_USER || db_config.generic.username,
  DB_PASS: process.env.DB_PASS || db_config.generic.password,
  DB_NAME: process.env.DB_NAME || db_config.generic.database,
  API_HOST: process.env.API_HOST || '0.0.0.0',
  API_PORT: process.env.API_PORT || 3002,
};
