
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction((t) => Promise.all([
    queryInterface.addColumn('Locations', 'long', {
      type: Sequelize.DataTypes.STRING,
    }, { transaction: t }),
    queryInterface.addColumn('Cases', 'status', {
      type: Sequelize.DataTypes.STRING,
      allowNull: false,
    }, { transaction: t }),
  ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize.transaction((t) => Promise.all([
    queryInterface.removeColumn('Locations', 'long', { transaction: t }),
    queryInterface.removeColumn('Cases', 'status', { transaction: t }),
  ])),
};
