
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction((t) => Promise.all([
    queryInterface.addColumn('Locations', 'lat', {
      type: Sequelize.DataTypes.STRING,
    }, { transaction: t }),
    queryInterface.addColumn('Cases', 'date', {
      type: Sequelize.DataTypes.DATE,
      allowNull: false,
    }, { transaction: t }),
    queryInterface.addColumn('Cases', 'count', {
      type: Sequelize.DataTypes.INTEGER,
      defaultValue: () => 0,
      allowNull: false,
    }, { transaction: t }),
  ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize.transaction((t) => Promise.all([
    queryInterface.removeColumn('Locations', 'lat', { transaction: t }),
    queryInterface.removeColumn('Cases', 'date', { transaction: t }),
    queryInterface.removeColumn('Cases', 'count', { transaction: t }),
  ])),
};
