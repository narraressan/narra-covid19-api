/* eslint-disable */
const supertest = require('supertest');
const app = require('../app/index.js');

// start and clean dependencies
beforeAll((done) => {
  server = app.listen(done);
  request = supertest.agent(server);
});
afterAll((done) => {
  server.close(done);
});

describe('Ping health endpoint', () => {
  it('Should receive a valid response', async () => {
    const res = await request.get(`/health`).send();
    expect(res.statusCode).toEqual(200);
  });
});

describe('Test if graphql gui is accessible', () => {
  it('Should receive a file response', async () => {
    const res = await request
                  .post(`/graphql`)
                  .send({
                    query: "{ Cases { _uuid } }",
                    variables: null
                  });
    
    expect(res.statusCode).toEqual(200);
  });
});
