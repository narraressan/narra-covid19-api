const _ = require('lodash');
const Case = require('../models/case');
const Location = require('../models/location');

module.exports = {
  async Location({ uuid }) {
    return await Location.findOne({ where: { _uuid: uuid } });
  },
  async Locations({ limit, offset }) {
    return await Location.findAll({
      limit,
      offset,
    });
  },
  async Case({ uuid }) {
    return await Case.findOne({
      where: { _uuid: uuid },
      include: [Location],
    });
  },
  async Cases({ limit, offset }) {
    return await Case.findAll({
      limit,
      offset,
      include: [Location],
    });
  },

  // mutaion functions
  async insertLocation(input) {
    try {
      await Location.create(input);
      return true;
    } catch (err) {
      return false;
    }
  },
  async deleteLocation({ uuid }) {
    return await Location.destroy({ where: { _uuid: uuid } });
  },
  async insertCase(input) {
    try {
      await Case.create(input);
      return true;
    } catch (err) {
      return false;
    }
  },
  async deleteCase({ uuid }) {
    return await Case.destroy({ where: { _uuid: uuid } });
  },
};
