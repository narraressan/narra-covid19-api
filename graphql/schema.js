const { buildSchema } = require('graphql');

module.exports = buildSchema(`
  type Location {
    _uuid: String
    country: String
    province: String
    lat: String
    long: String
  }
  type Case {
    _uuid: String
    location_uuid: String
    Location: Location
    date: String
    count: Int
    status: String
  }
  type Query {
    Location(uuid: String): Location
    Locations(limit: Int = 10, offset: Int = 0): [Location]
    Case(uuid: String): Case
    Cases(limit: Int = 10, offset: Int = 0): [Case]
  }
  type Mutation {
    insertLocation(country: String, province: String, lat: String, long: String): Boolean
    deleteLocation(uuid: String): Boolean
    insertCase(location_uuid: String, date: String, count: Int, status: String): Boolean
    deleteCase(uuid: String): Boolean
  }
`);
