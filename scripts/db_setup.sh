#!/bin/bash
if [ "$DB_HOST" = "" ]
then
    DB_HOST=localhost
    DB_PORT=5432
else
    DB_HOST=$DB_HOST
    DB_PORT=$DB_PORT
fi

for count in {1..100}; do
    echo "db connect attempt "${count}
    if  $(nc -z ${DB_HOST} ${DB_PORT}) ; then
        npm run db-setup
        break
    fi
    sleep 5
done