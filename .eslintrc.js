module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    'class-methods-use-this': 0,
    'no-restricted-syntax': 0,
    'guard-for-in': 0,
    'no-unused-vars': 1,
    'import/no-dynamic-require': 1,
    'no-shadow': 1,
    'no-underscore-dangle': 1,
    'camelcase': 1,
    'no-return-await': 1,
  },
};