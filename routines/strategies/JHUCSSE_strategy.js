// Crawl data from `https://github.com/CSSEGISandData/COVID-19`

const moment = require('moment');
const axios = require('axios');
const papa = require('papaparse');
const _ = require('lodash');

const Case = require('../../models/case');
const Location = require('../../models/location');

const BaseStrategy = require('./base_strategy');

class JHUCSSEStrategy extends BaseStrategy {
  constructor() {
    super();

    this.CONFIRMED_CASES_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv';
    this.DEATHS_CASES_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv';
    this.RECOVERED_CASES_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv';
  }

  async fetchSource(url) {
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  parseSource(data) {
    const parsedData = papa.parse(data, { delimiter: ',', header: true });
    return parsedData.data;
  }

  async storeSource(summary = []) {
    const locations = {};

    await _.each(summary, async (row) => {
      const cases = [];

      const tmpLatLong = `${row.long}-${row.lat}`;
      if (!locations[tmpLatLong]) {
        locations[tmpLatLong] = await this.fetchLocation(row);
      }

      await _.forOwn(row.cases, async (val, key) => {
        const newCase = {
          location_uuid: locations[tmpLatLong],
          date: moment(key).format('YYYY-M-D'),
          ...val,
        };
        cases.push(newCase);
      });

      try {
        console.log(`Insert ${cases.length} cases for location ${locations[tmpLatLong]}`);
        await Case.bulkCreate(cases, { ignoreDuplicates: true });
      } catch (err) {
        console.log(err);
      }
    });
  }

  async fetchLocation(location) {
    try {
      const loc = await Location.findOne({ where: { lat: location.lat, long: location.long } });
      if (loc !== null) {
        return loc._uuid;
      }
    } catch (err) {
      console.log(err);
    }

    return null;
  }

  async registerLocations(locations) {
    try {
      await Location.bulkCreate(locations, { ignoreDuplicates: true });
    } catch (err) {
      console.log(err);
    }
  }

  fetchIncludedDates(start, end) {
    const dformat = 'M/D/YY';
    const currDate = start.startOf('day');
    const lastDate = end.startOf('day');
    const dates = [currDate.clone().format(dformat)];

    while (currDate.add(1, 'days').diff(lastDate) < 0) {
      const date = currDate.clone().format(dformat);
      dates.push(date);
    }

    return dates;
  }

  async summarizeCases(dates, rawData, type) {
    const summary = [];
    const locations = [];

    _.each(rawData, (row) => {
      if (![row['Country/Region'], row.Lat, row.Long].includes(undefined)) {
        const loc = {
          country: row['Country/Region'],
          province: row['Province/State'] !== '' ? row['Province/State'] : null,
          lat: row.Lat,
          long: row.Long,
        };
        locations.push(loc);

        const tmp = { ...loc, cases: {} };

        _.each(dates, (date) => {
          tmp.cases[date] = {
            status: type, // this should be from a constant enum
            count: row[date] ? parseInt(row[date], 10) : 0,
          };
        });

        summary.push(tmp);
      }
    });

    await this.registerLocations(locations);

    return summary;
  }

  async start(start = null, end = null) {
    const today = moment();
    const startDate = (start == null) ? today : moment(start);
    const endDate = (end == null) ? today : moment(end);

    const dates = this.fetchIncludedDates(startDate, endDate);
    let summary = [];

    // fetch CONFIRMED cases
    const confirmedCasesData = this.parseSource(await this.fetchSource(this.CONFIRMED_CASES_URL));
    summary = _.concat(summary, await this.summarizeCases(dates, confirmedCasesData, 'NEW_CONFIRMED'));

    // fetch DEATH cases
    const deathCasesData = this.parseSource(await this.fetchSource(this.DEATHS_CASES_URL));
    summary = _.concat(summary, await this.summarizeCases(dates, deathCasesData, 'DEATH'));

    // fetch RECOVERED
    const recoveredCasesData = this.parseSource(await this.fetchSource(this.RECOVERED_CASES_URL));
    summary = _.concat(summary, await this.summarizeCases(dates, recoveredCasesData, 'RECOVERED'));

    await this.storeSource(summary);
  }
}

module.exports = JHUCSSEStrategy;
