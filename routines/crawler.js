const strategies = require('./strategies');

class CovidCrawler {
  collectData(strategy) {
    const crawler = new strategies[strategy]();

    // should be in YYYY-MM-DD format
    crawler.start('2020-01-01');
  }
}

const crawler = new CovidCrawler();
for (const strategy in strategies) {
  crawler.collectData(strategy);
}
