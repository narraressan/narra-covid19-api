const http = require('http');
const Koa = require('koa');
const koaBody = require('koa-body');
const cors = require('@koa/cors');
const graphqlHTTP = require('koa-graphql');
const mount = require('koa-mount');
const schema = require('../graphql/schema');
const root = require('../graphql/root');
const { log } = require('../utils/helpers');


// internal dependencies
const healthAPI = require('./apis/health');


// default config
const { API_HOST, API_PORT } = require('../config');


const app = new Koa();


// initialize middlewares
app.use(koaBody());
app.use(cors());
app
  .use(healthAPI.routes())
  .use(
    mount(
      '/graphql',
      graphqlHTTP({
        schema,
        rootValue: root,
        graphiql: true,
      }),
    ),
  )
  .use(healthAPI.allowedMethods());


// start the server
const server = http.createServer(app.callback());
server.listen(API_PORT, API_HOST, () => { log.info(`payment-api::init@${API_HOST}:${API_PORT}`); });


module.exports = app;
