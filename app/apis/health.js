const Router = require('koa-router');

const api = new Router({ prefix: '/health' });
// const {
//   ServerException,
//   StripeServiceFailedException
// } = require('../utils/exceptions');


/**
 * [POST] `/health`
 * - check health
 *
 * @auth NA
 * @query NA
 * @params NA
 * @body NA
 * @success {
 *    {integer} code,
 *    {string} status,
 *    {string} message
 * }
 * @error {
 *    {integer} code,
 *    {string} error,
 *    {string} message
 * }
 */
api.get('/', async (ctx) => {
  ctx.response.status = 200;
  ctx.body = {
    code: 200,
    status: 'SERVICE_IS_ALIVE',
    message: 'Service is alive and healthy',
  };
});


module.exports = api;
