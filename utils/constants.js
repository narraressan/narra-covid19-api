module.exports = {
  CASES_TYPE: {
    NEW: 'NEWLY_CONFIRMED',
    DEATH: 'DEATH',
    RECOVERED: 'RECOVERED',
  },
};
