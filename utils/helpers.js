const { createLogger, format, transports } = require('winston');
const uuidv4 = require('uuid/v4');


const { combine, timestamp } = format;
const path = require('path');


const log = createLogger({
  format: combine(
    timestamp(),
    format.printf(({ level, message, timestamp }) => `${timestamp} ${level}: ${message}`),
  ),
  transports: [
    new transports.Console(),
    new transports.File({ filename: path.join(__dirname, '../logs/logs.log') }),
  ],
});


const sleep = (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});


// get safe uuid text
const getUUID = () => uuidv4();


module.exports = { log, sleep, getUUID };
