const { Model, DataTypes } = require('sequelize');
const db = require('./db_connect');
const Case = require('./case');
const { getUUID } = require('../utils/helpers');

class Location extends Model {}
Location.init({
  _uuid: {
    type: DataTypes.UUID,
    defaultValue: getUUID,
    unique: true,
  },
  country: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  province: {
    type: DataTypes.STRING,
  },
  lat: {
    type: DataTypes.STRING,
    primaryKey: true,
  },
  long: {
    type: DataTypes.STRING,
    primaryKey: true,
  },
}, {
  sequelize: db,
  timestamps: true,
});

module.exports = Location;
