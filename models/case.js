const { Model, DataTypes } = require('sequelize');
const db = require('./db_connect');
const Location = require('./location');
const { getUUID } = require('../utils/helpers');

class Case extends Model {}
Case.init({
  _uuid: {
    type: DataTypes.UUID,
    defaultValue: getUUID,
    unique: true,
  },
  location_uuid: {
    type: DataTypes.UUID,
    allowNull: false,
    references: {
      model: Location,
      key: '_uuid',
    },
    primaryKey: true,
  },
  date: {
    type: DataTypes.DATE,
    allowNull: false,
    primaryKey: true,
  },
  count: {
    type: DataTypes.INTEGER,
    defaultValue: () => 0,
    allowNull: false,
  },
  status: {
    type: DataTypes.STRING,
    allowNull: false,
    primaryKey: true,
  },
}, {
  sequelize: db,
  timestamps: true,
});
Case.hasOne(Location, { foreignKey: '_uuid' });

module.exports = Case;
