const Case = require('./case');
const Location = require('./location');

const createTables = async () => {
  // create the tables using sequelize
  // Note: enableAlter should be false, if you need to alter, then use the migration script
  const enableAlter = false;
  await Location.sync({ alter: enableAlter });
  await Case.sync({ alter: enableAlter });
};

(async () => {
  await createTables();
})();
