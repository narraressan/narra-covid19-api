# narra-covid19-api

A simple COVID-19 API where data are scraped from `Johns Hopkins CSSE` a very trusted and reliable data source.

The main goal of this project is to create a GraphQL based service to serve the data scraped from `Johns Hopkins CSSE`.


### Setup local dev
``` bash
# install dependencies
npm install --verbose

# start the application at localhost:3002
# note that the application is served with pm2 ecosystem.
npm start
npm run logs

# you may run the API or the crawlers alone w/ the commands bellow
npm run apis
npm run crawlers

# stop and deletes the service/s running in pm2
npm run stop

# to run tests, simply execute
npm run tests

# to execute db setup for fresh projects
npm run db-setup

# for projects that require revision of tables instead of fresh build
sequelize migration:generate --name [name-of-migration-file]
sequelize db:migrate --env generic
sequelize db:migrate:undo --env generic

# run and fix linter manually. 
# this should also be triggered during pre-commit
npm run linter

# run dev via docker-compose - highly recommended!
docker-compose --file ./docker/dev/dev.yml up -d --build

# view logs
docker-compose --file ./docker/dev/dev.yml logs -f --tail 10
```
 
Read more about pm2 ecosystem [here](https://pm2.keymetrics.io/docs/usage/application-declaration/) as it is the core process manager of this application. It manages both the scheduled crawlers and the API.

Also, the graphql wrapper used is `koa-graphql`. Read the about it [here](https://www.npmjs.com/package/koa-graphql) and a quick tutorial [here](https://hellocode.dev/graphql-server)

Lastly, the main framework used to serving the RESTful service is `Koa Js`. Find out more about it [here](https://koajs.com/)


### Build
``` bash
# docker for prod
docker-compose --file ./docker/prod/prod.yml build
```


### Publish docker
``` bash
# login using specific user docker login docker.io -u username -p password
docker login docker.io

# publish the image to your docker repository
docker tag prod_narra-covid19-api [username]/narra-covid19-api:latest
docker push [username]/narra-covid19-api:latest

# logout from docker
docker logout
```

Note:

Check the file `./docker/prod/test.yml` for reference on how to call the image.


### Future releases
- use mock testing
- improve data served by adding patient records
- make more integrated with participation of real users
- write a UI to make use of the data


> more to come soon probably a full blown service derived from this simple project ...